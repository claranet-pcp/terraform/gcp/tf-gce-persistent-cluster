resource "google_compute_address" "public_address" {
  count = "${length(split(",", var.zones))}"

  name = "${var.service}-${var.identifier}-${var.gcp_region}-${count.index}-address"
}

resource "google_compute_disk" "disk" {
  count = "${length(split(",", var.zones))}"

  lifecycle {
    create_before_destroy = true
  }

  name = "${var.service}-${var.identifier}-persistent-${element(split(",", var.zones), count.index)}"
  type = "${var.disk_type}"
  zone = "${element(split(",", var.zones), count.index)}"
  size = "${var.disk_size}"
}

resource "google_compute_instance_template" "instance_template" {
  count = "${length(split(",", var.zones))}"

  lifecycle {
    create_before_destroy = true
  }

  name_prefix    = "${var.service}-${var.identifier}-${var.gcp_region}-${count.index}-${var.envname}"
  can_ip_forward = "${var.ip_forward}"
  machine_type   = "${var.machine_type}"
  region         = "${var.gcp_region}"

  tags = [
    "${var.envname}",
    "${var.service}",
    "${var.needs_nat}",
    "${var.fw_tags}",
  ]

  disk {
    device_name  = "${var.disk_device_name}"
    source_image = "${var.disk_image}"
    boot         = true
  }

  disk {
    source      = "${var.service}-${var.identifier}-persistent-${element(split(",", var.zones), count.index)}"
    auto_delete = false
    device_name = "${var.persistent_disk_name}"
  }

  network_interface {
    subnetwork = "${var.net_name}"

    access_config {
      nat_ip = "${element(google_compute_address.public_address.*.address, count.index)}"
    }
  }

  metadata {
    envname            = "${var.envname}"
    profile            = "${var.service}"
    role               = "${var.service}"
    cluster            = "${var.identifier}"
    domain             = "${var.domain}"
    startup-script-url = "${var.startup_script}"
  }

  service_account {
    scopes = ["${var.scopes}"]
  }
}

resource "google_compute_instance_group_manager" "instance_group_manager" {
  count = "${length(split(",", var.zones))}"

  name               = "${var.service}-${var.identifier}-${var.gcp_region}-${count.index}-${var.envname}"
  instance_template  = "${element(google_compute_instance_template.instance_template.*.self_link, count.index)}"
  base_instance_name = "${var.service}-${var.identifier}-${var.gcp_region}-${count.index}-${var.envname}"
  update_strategy    = "${var.update_strategy}"
  zone               = "${element(split(",", var.zones), count.index)}"
}

resource "google_dns_record_set" "dns_a_record" {
  count = "${length(split(",", var.zones))}"

  name         = "${var.service}-${var.identifier}-${count.index}-${var.gcp_region}.${var.domain}."
  managed_zone = "${var.managed_dns}"
  type         = "A"
  ttl          = "${var.ttl}"
  rrdatas      = ["${element(google_compute_instance_template.instance_template.*.network_interface.0.access_config.0.nat_ip, count.index)}"]
}
