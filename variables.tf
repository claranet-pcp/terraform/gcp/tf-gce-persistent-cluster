/* Environment variables */
variable "envname" {
  description = "This will be interpolated into the name for all resources in last position, also the value for the 'envname' metadata label as well as being adde as a networking tag"
  type = "string"
}

variable "service" {
  description = "This will be interpolated into the name for all resources in position 1, as well as being added to the 'profile' and 'role' labels"
  type = "string"
}

variable "identifier" {
  description = "This will be interpolated into the name for all resources in position 2, as well as being added to the 'cluster' instance metadata label"
  type = "string"
}

variable "domain" {
  description = "This will be interpolated into the name for the DNS record set in last position as well as becoming the value for the 'domain' instance metadata label"
  type = "string"
}

/* GCP Region and Zone */
variable "gcp_region" {
  description = "The GCP region you wish to create these resources in"
  type = "string"
  default = "europe-west1"
}

variable "zones" {
  description = "List of availability zones to create managed instances in"
  type = "string"
}

variable "nodes_per_zone" {
  description = "Count of how many instances you would like to create per zone"
  type = "string"
  default = 1
}

/* DNS variables */
variable "managed_dns" {
  description = "The name of the managed DNS zone to create records in for resources created by this module"
  type = "string"
}

variable "ttl" {
  description = "The TTL for DNS records created by this module"
  type = "string"
  default = "300"
}

/* Instance variables */
variable "machine_type" {
  description = "The GCP machine type to use for the instance template"
  type = "string"
}

variable "disk_image" {
  description = "The disk image to use for the instance template"
  type = "string"
}

variable "startup_script" {
  description = "The content for the instance startup script"
  type = "string"
}

variable "ip_forward" {
  description = "Bool indicating whether instances can IP forward"
  type = "string"
  default = true
}

variable "disk_type" {
  description = "The disk type to use for instances created from the instance template"
  type = "string"
  default = "pd-ssd"
}

variable "disk_size" {
  description = "The disk size to use for instances created from the instance template"
  type = "string"
  default = 100
}

variable "disk_device_name" {
  description = "The disk device name to use for instances created from the instance template"
  type = "string"
  default = "sda"
}

variable "scopes" {
  description = "List of security scopes to use for instances created from the instance template"
  type = "list"
  default = [
    "storage-ro",
    "compute-ro",
  ]
}

variable "persistent_disk_name" {
  description = "The disk device name to use for persistent disks"
  type = "string"
  default = "sdc"
}

variable "update_strategy" {
  description = "How a managed group handles a change to the underlying template, RESTART or NONE"
  type = "string"
  default = "NONE"
}

/* Network variables */
variable "fw_tags" {
  description = "A list of networking tags to add to instances created from the instance template"
  type = "list"
}

variable "needs_nat" {
  description = "Indicates whether NAT is required, blank to remove NAT from the instance template"
  type = "string"
  default = "nat"
}

variable "net_name" {
  description = "The name of the subnetwork to connect instances to"
  type = "string"
}
